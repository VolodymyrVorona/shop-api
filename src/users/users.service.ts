import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from './users.model';
import { CreateUserDto } from './dto/create-user.dto';
import { hash } from 'bcrypt';
import { TFilter } from './types';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User) private userModel: typeof User) {}

  findOne(filter: TFilter): Promise<User> {
    return this.userModel.findOne({ ...filter });
  }

  async create(
    createUserDto: CreateUserDto,
  ): Promise<User | { warningMessage: string }> {
    const usernameExists = await this.findOne({
      where: { username: createUserDto.username },
    });

    const emailExists = await this.findOne({
      where: { email: createUserDto.email },
    });

    if (usernameExists) {
      return {
        warningMessage: `user with username ${usernameExists.username} already exists`,
      };
    }
    if (emailExists) {
      return {
        warningMessage: `user with email ${emailExists.email} already exists`,
      };
    }

    const hashedPassword = await hash(createUserDto.password, 10);
    const user = await User.create({
      username: createUserDto.username,
      email: createUserDto.email,
      password: hashedPassword,
    });

    const parcedUser: User = user.toJSON();
    delete parcedUser.password;

    return parcedUser;
  }
}
