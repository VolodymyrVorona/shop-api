import { ApiProperty } from '@nestjs/swagger';

export class LoginUserRequest {
  @ApiProperty({ example: 'volodymyr' })
  username: string;

  @ApiProperty({ example: 'volodymyr' })
  password: string;
}

export class LoginUserResponse {
  @ApiProperty({
    example: {
      userId: 1,
      username: 'volodymyr',
      email: 'volodymyr@gmail.com',
    },
  })
  user: {
    userId: number;
    username: string;
    email: string;
  };

  @ApiProperty({ example: 'Logged in' })
  msg: string;
}

export type TFilter = {
  where: {
    id?: string;
    username?: string;
    email?: string;
  };
};

export class LogoutUserResponse {
  @ApiProperty({ example: 'session has ended' })
  msg: string;
}

export class LoginCheckResponse {
  @ApiProperty({ example: 1 })
  userId: number;

  @ApiProperty({ example: 'volodymyr' })
  username: string;

  @ApiProperty({ example: 'volodymyr@gmail.com' })
  email: string;
}

// {
//     "id": 4,
//     "username": "Ivan",
//     "password": "$2b$10$VoOnkGQz6cRjSZiFAeIMmeliWopZxARy7F4R.slBkrLojkifiXD/.",
//     "email": "ivan@gmail.com",
//     "updatedAt": "2023-11-01T16:59:42.330Z",
//     "createdAt": "2023-11-01T16:59:42.330Z"
// }

export class SignupResponse {
  @ApiProperty({ example: 1 })
  id: number;

  @ApiProperty({ example: 'Ivan' })
  username: string;

  @ApiProperty({ example: 'ivan@gmail.com' })
  email: string;
}
