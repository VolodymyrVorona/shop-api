import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({ example: 'Volodymyr' })
  @IsNotEmpty()
  readonly username: string;

  @ApiProperty({ example: 'Volodymyr' })
  @IsNotEmpty()
  readonly password: string;

  @ApiProperty({ example: 'volodymyr@gmail.com' })
  @IsNotEmpty()
  readonly email: string;
}
